<?php 
  use app\models\Pelayanan; 
?>
<script>

FusionCharts.ready(function(){
      var revenueChart = new FusionCharts({
        "type": "Column3d",
        "renderAt": "grafik-pelayanan-per-status",
        "width": "100%",
        "height": "300",
        "dataFormat": "json",
        "dataSource": {
          "chart": {
              "caption" : "Grafik Pelayanan per Status",
              "xAxisName": "Pelayanan per Status",
              "yAxisName": "Jumlah",
              "theme": "fint"
           },
          "data":        
              [ <?php print Pelayanan::getGrafikPerStatus(); ?> ]
        }
    });
    revenueChart.render();
})
		
</script> 
<div id="grafik-pelayanan-per-status"> FusionChart XT will load here! </div>