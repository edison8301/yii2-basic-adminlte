<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use PhpOffice\PhpWord\Shared\Converter;

use app\models\LoginForm;
use app\models\User;
use app\models\ContactForm;
use app\models\Pelayanan;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionSetRating($kode_pelacakan)
    {
        $model = Pelayanan::find()
            ->andWhere(['kode_pelacakan' => $kode_pelacakan])
            ->one();

        $rating = Yii::$app->request->post('rating');

        $model->rating = $rating;
        if($model->save(false)){
            Yii::$app->session->setFlash('success','Pelayanan Berhasil diberi rating !');
            return $this->redirect(Yii::$app->request->referrer);
        }        
    }

    public function actionSetKomentar($kode_pelacakan)
    {
        $model = Pelayanan::find()
            ->andWhere(['kode_pelacakan' => $kode_pelacakan])
            ->one();

        $komentar = Yii::$app->request->post('komentar');

        $model->komentar = $komentar;
        if($model->save(false)){
            Yii::$app->session->setFlash('success','Pelayanan Berhasil diberi komentar !');
            return $this->redirect(Yii::$app->request->referrer);
        }        
    }    

    /**
     * Displays homepage.
     *
     * @return string
     */

    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['admin/index']);
        } else{
            return $this->redirect(['site/login']);
        }
    }

    public function actionRegulasi()
    {
        $this->layout = 'frontend';
        return $this->render('regulasi');        
    }

    public function actionPengajuanPelayanan($id_pelayanan_jenis)
    {
        $this->layout = 'frontend';

        $model = new Pelayanan();

        $model->id_pelayanan_jenis = $id_pelayanan_jenis;

        if ($model->load(Yii::$app->request->post())) {
            $berkas_permohonan = UploadedFile::getInstance($model, 'berkas_permohonan');
            if ($berkas_permohonan !== null) {
                $model->berkas_permohonan = $berkas_permohonan->baseName . Yii::$app->formatter->asTimestamp(date('Y-d-m h:i:s')) . '.' . $berkas_permohonan->extension;
            }
            if ($model->save()) {
                //$model->sendMail();

                if ($berkas_permohonan!==null) {
                    $path = Yii::getAlias('@app').'/web/berkas/';
                    $berkas_permohonan->saveAs($path.$model->berkas_permohonan, false);
                }

                Yii::$app->session->setFlash('success','Pegajuan berhasil dikirim. Kode pelacakan Anda adalah <b>'.$model->kode_pelacakan.'</b> yang dapat digunakan untuk memeriksa status permohonan pelayanan Anda.');
                return $this->redirect(['site/index']);
            }
            Yii::$app->session->setFlash('error','Data gagal disimpan. Silahkan periksa kembali isian Anda.');

        }

        return $this->render('pengajuanPelayanan', [
            'model' => $model,
            'id_pelayanan_jenis' => $id_pelayanan_jenis
        ]);
    }

    public function actionGetBerkas($berkas)
    {        
        $path = Yii::getAlias('@web') . '/sop/'.$berkas;
        return $this->redirect($path);
    }

    public function actionTentang()
    {
        $this->layout = 'frontend';
        return $this->render('tentang');
    }

    public function actionPencarian($kode_pelacakan)
    {
        $this->layout = 'frontend';
        $model = Pelayanan::find()
            ->andWhere(['kode_pelacakan' => $kode_pelacakan])
            ->one();

        if ($model !==null){
            return $this->render('pencarian',['model' => $model]);
        } else{
            Yii::$app->session->setFlash('warning','Kode pelacakan tidak ditemukan !');
            return $this->redirect(['site/index']);
        }
    }

    public function actionLogin()
    {
        $this->layout = '/frontend/login';

        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['admin/index']);
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['admin/index']);
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }


}
