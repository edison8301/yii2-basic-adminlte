<?php 
  use app\models\Pelayanan; 
?>
<script>

FusionCharts.ready(function(){
      var revenueChart = new FusionCharts({
        "type": "Pie3d",
        "renderAt": "grafik-pelayanan-per-jenis",
        "width": "100%",
        "height": "300",
        "dataFormat": "json",
        "dataSource": {
          "chart": {
              "caption" : "Grafik Pelayanan per Jenis",
              "xAxisName": "Pelayanan per Jenis",
              "yAxisName": "Jumlah",
              "showLabels": "0",

              "showLegend": "1",
              "theme": "fint",
              "showPercentValues": "1",
              "showPercentInToolTip": "0",
              //Setting legend to appear on right side
              "legendPosition": "top",
              //Caption for legend
              "legendCaption": "Alphabet Used: ",
              //Customization for legend scroll bar cosmetics
              "legendScrollBgColor": "#cccccc",
              "legendScrollBarColor": "#999999"
           },
          "data":        
              [ <?php print Pelayanan::getGrafikPerJenis(); ?> ]
        }
    });
    revenueChart.render();
})
		
</script> 
<div id="grafik-pelayanan-per-jenis"> FusionChart XT will load here! </div>