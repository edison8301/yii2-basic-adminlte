<?php

use yii\helpers\Html;
use yii\helpers\Url; 




$this->title = "Halaman Dashboard";

?>

<div class="row">
	
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-aqua">
			<div class="inner">
				<p>Menunggu Diproses</p>
				
				<h3>8</h3>
			</div>
			<div class="icon">
				<i class="fa fa-refresh"></i>
			</div>
			<a href="<?= Url::to(['pelayanan/index','id_pelayanan_status'=>2]); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>

	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-purple">
			<div class="inner">
				<p>Dalam Proses</p>
				
				<h3>8</h3>
			</div>
			<div class="icon">
				<i class="fa fa-clock-o"></i>
			</div>
			<a href="<?= Url::to(['pelayanan/index','id_pelayanan_status'=>3]); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>

	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-green">
			<div class="inner">
				<p>Pelayanan Selesai</p>

				<h3>8</h3>
			</div>
			<div class="icon">
				<i class="fa fa-check-square-o"></i>
			</div>
			<a href="<?= Url::to(['pelayanan/index','id_pelayanan_status'=>1]); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>

	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-red">
			<div class="inner">
				<p>Pelayanan Ditolak</p>
				
				<h3>8</h3>
			</div>
			<div class="icon">
				<i class="fa fa-remove"></i>
			</div>
			<a href="<?= Url::to(['pelayanan/index','id_pelayanan_status'=>4]); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Permohonan Pelayanan per Bulan</h3>
			</div>
			<div class="box-body">
				<?= $this->render('_grafikPermohonanPerBulan'); ?>
			</div>
		</div>
	</div>
</div>

