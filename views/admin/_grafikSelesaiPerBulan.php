<?php 
  use app\models\Pelayanan; 
?>
<script>

FusionCharts.ready(function(){
      var revenueChart = new FusionCharts({
        "type": "Column3d",
        "renderAt": "grafik-selesai-per-bulan",
        "width": "100%",
        "height": "300",
        "dataFormat": "json",
        "dataSource": {
          "chart": {
              "caption" : "Grafik Penyelesaian Pelayanan per Bulan",
              "xAxisName": "Penyelesaian Pelayanan per Bulan",
              "yAxisName": "Jumlah",
              "theme": "fint"
           },
          "data":        
              [ <?php print Pelayanan::getGrafikSelesaiPerBulan(); ?> ]
        }
    });
    revenueChart.render();
})
		
</script> 
<div id="grafik-selesai-per-bulan"> FusionChart XT will load here! </div>