<?php 
  use app\models\Pelayanan;
?>
<script>

FusionCharts.ready(function(){
      var revenueChart = new FusionCharts({
        "type": "Column3d",
        "renderAt": "grafik-permohonan-per-bulan",
        "width": "100%",
        "height": "300",
        "dataFormat": "json",
        "dataSource": {
          "chart": {
              "caption" : "Grafik Permohonan per Bulan",
              "xAxisName": "Permohonan per Bulan",
              "yAxisName": "Jumlah",
              "theme": "fint"
           },
          "data":        
              [  ]
        }
    });
    revenueChart.render();
})
		
</script> 
<div id="grafik-permohonan-per-bulan"> FusionChart XT will load here! </div>