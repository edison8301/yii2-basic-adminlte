<?php

namespace app\controllers;

class AdminController extends \yii\web\Controller
{
    public $layout = '/backend/main';

    public function actionIndex()
    {
        return $this->render('index');
    }

}
